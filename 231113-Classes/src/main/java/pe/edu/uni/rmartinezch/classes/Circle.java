/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.rmartinezch.classes;

/**
 *
 * @author PROFESOR
 */
public class Circle {
    
    public static void main(String[] args) {
        System.out.println("Circle !!!");
    }

    double radious;

    Circle() {
        this.radious = 1;
    }

    Circle(double newRadious) {
        this.radious = newRadious;
    }

    double getArea() {
        return Math.PI * this.radious * this.radious;
    }

    double getPerimeter() {
        return 2 * Math.PI * this.radious;
    }
    
    void setRadious(double radious){
        this.radious = radious;
    }

    @Override
    public String toString() {
        return "Circle{" + "radious=" + radious + '}';
    }
    
}
