/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.rmartinezch.abstractclasses;

import java.util.Date;

/**
 *
 * @author Ronald Martinez <rmartinezch@uni.edu.pe>
 */
public abstract class GeometricObject {

    private String color;
    private boolean filled;
    private java.util.Date createdDate;

    protected GeometricObject() {
        color = "white";
        filled = false;
        createdDate = new java.util.Date();
    }

    protected GeometricObject(String color, boolean filled) {
        createdDate = new java.util.Date();
        this.color = color;
        this.filled = filled;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    @Override
    public String toString() {
        return "GeometricObject{" + "color=" + color + ", filled=" + filled + ", createdDate=" + createdDate + '}';
    }
    /*
    public double getArea(){
        return 0.0;
    }
    */

    public abstract double getArea();
    public abstract double getPerimeter();
}
