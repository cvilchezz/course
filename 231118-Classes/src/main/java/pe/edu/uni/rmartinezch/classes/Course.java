/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.rmartinezch.classes;

/**
 *
 * @author Ronald Martinez <rmartinezch@uni.edu.pe>
 */
public class Course {
    private String courseName;
    private String[] students;
    private int numberOfStudents;
    private final int MAX_STUDENTS = 100;   // definimos el máximo de estudiantes
    
    public Course(String courseName){
        this.courseName = courseName;
        students = new String[MAX_STUDENTS]; 
    }

    public String getCourseName() {
        return courseName;
    }

    public int getNumberOfStudents() {
        return numberOfStudents;
    }

    public String[] getStudents() {
        return students;
    }
    
    void addStudent(String student){
        students[numberOfStudents] = student;
        numberOfStudents++;
    }
    
    void dropStudents(String student){
        // 
    }
}
