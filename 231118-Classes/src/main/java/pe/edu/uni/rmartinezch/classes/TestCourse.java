/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.edu.uni.rmartinezch.classes;

/**
 *
 * @author Ronald Martinez <rmartinezch@uni.edu.pe>
 */
public class TestCourse {

    public static void main(String[] args) {
        System.out.println("TestCourse!");
        String[] cursos = {"Estructuras de datos", "Sistemas de base de datos", "Programación orientada a objetos"};
        String[] alumnos = {"Bryan Cucho", "Luis Sanchez", "Anthony Alva"};
        
        // Matricula inicialización
        Course course1 = new Course(cursos[0]);
        Course course2 = new Course(cursos[1]);
        Course course3 = new Course(cursos[2]);
        
        // Matricula carga de alumnos
        course1.addStudent(alumnos[0]);
        course1.addStudent(alumnos[2]);

        course2.addStudent(alumnos[1]);
        course2.addStudent(alumnos[2]);

        course3.addStudent(alumnos[0]);
        course3.addStudent(alumnos[1]);
        
        // Matricula reportes
        System.out.println("Número de alumnos en el curso " + course1.getCourseName() + ": " + course1.getNumberOfStudents());
        viewStudents(course1);
        System.out.println("Número de alumnos en el curso " + course2.getCourseName() + ": " + course2.getNumberOfStudents());
        viewStudents(course2);
        System.out.println("Número de alumnos en el curso " + course3.getCourseName() + ": " + course3.getNumberOfStudents());
        viewStudents(course3);
    }
    
    private static void viewStudents(Course course){
        String[] alumnos = course.getStudents();
        for (int i = 0; i < course.getNumberOfStudents(); i++) {
            System.out.print(alumnos[i] + ", ");
        }
        System.out.println("");
    }
}
